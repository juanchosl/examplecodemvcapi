package actions

import (
	"ExampleCodeMVCApi/src/entity"
	"ExampleCodeMVCApi/src/storage"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

func SetPerson(c *gin.Context) {
	paciente := entity.Patient{
		Name:  c.PostForm("name"),
		Email: c.PostForm("email"),
	}
	result := storage.DB.Create(&paciente)
	if result.Error != nil {
		c.IndentedJSON(http.StatusNotAcceptable, gin.H{"error": result.Error})
	} else {
		//storage.DB.Last(&paciente)
		c.IndentedJSON(http.StatusOK, paciente)
	}
}

func GetPerson(c *gin.Context) {

	var person entity.Patient
	id := c.Param("id")
	result := storage.DB.Find(&person, "id = ?", id)
	if result.RowsAffected == 0 {
		c.IndentedJSON(http.StatusNotFound, gin.H{"error": fmt.Sprintf("Person with ID: %s not found.", id)})
		//c.IndentedJSON(http.StatusNotFound, gin.H{"error": result.Error})
	} else {
		c.IndentedJSON(http.StatusOK, person)
	}
}

func GetPersons(c *gin.Context) {
	var persons []entity.Patient
	result := storage.DB.Find(&persons)
	if result.RowsAffected == 0 {
		c.IndentedJSON(http.StatusNotFound, gin.H{"error": result.Error})
	} else {
		c.IndentedJSON(http.StatusOK, persons)
	}
}

func UpdatePerson(c *gin.Context) {
	var person entity.Patient

	id := c.Param("id")
	reader := storage.DB.Find(&person, "id = ?", id)
	if reader.RowsAffected == 0 {
		c.IndentedJSON(http.StatusNotFound, gin.H{"error": fmt.Sprintf("Person with ID: %s not found.", id)})
	} else {
		//var data entity.Patient
		//data.Name = c.PostForm("name")
		//result := storage.DB.Model(person).Updates(data)
		person.Name = c.PostForm("name")
		result := storage.DB.Updates(&person)
		if result.RowsAffected == 0 {
			c.IndentedJSON(http.StatusNotModified, gin.H{"error": result.Error})
		} else {
			//storage.DB.Find(&person, "id = ?", id)
			c.IndentedJSON(http.StatusOK, person)
		}
	}
}

func DeletePerson(c *gin.Context) {
	var person entity.Patient

	id := c.Param("id")
	result := storage.DB.Delete(person, "id = ?", id)
	if result.RowsAffected == 0 {
		c.IndentedJSON(http.StatusNotFound, gin.H{"error": fmt.Sprintf("Person with ID: %s not found.", id)})
	} else {
		c.IndentedJSON(http.StatusResetContent, gin.H{"deleted": result.RowsAffected})
	}
}
