package actions

import (
	"ExampleCodeMVCApi/src/entity"
	"ExampleCodeMVCApi/src/storage"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

func SetDoctor(c *gin.Context) {
	doctor := entity.Doctor{
		Name:  c.PostForm("name"),
		Email: c.PostForm("email"),
	}
	result := storage.DB.Create(&doctor)
	if result.Error != nil {
		c.IndentedJSON(http.StatusOK, gin.H{"error": result.Error})
	} else {
		//storage.DB.Last(&doctor)
		c.IndentedJSON(http.StatusOK, doctor)
	}
}

func GetDoctor(c *gin.Context) {

	var doctor entity.Doctor

	id := c.Param("id")
	result := storage.DB.Find(&doctor, "id = ?", id)
	if result.RowsAffected == 0 {
		c.IndentedJSON(http.StatusNotFound, gin.H{"error": fmt.Sprintf("Doctor with ID: %s not found.", id)})
	} else {
		c.IndentedJSON(http.StatusOK, doctor)
	}
}

func GetDoctors(c *gin.Context) {
	var doctors []entity.Doctor
	result := storage.DB.Find(&doctors)
	if result.RowsAffected == 0 {
		c.IndentedJSON(http.StatusNotFound, gin.H{"error": result.Error})
	} else {
		c.IndentedJSON(http.StatusOK, doctors)
	}
}

func UpdateDoctor(c *gin.Context) {

	var doctor entity.Doctor

	id := c.Param("id")
	reader := storage.DB.Find(&doctor, "id = ?", id)
	if reader.RowsAffected == 0 {
		c.IndentedJSON(http.StatusNotFound, gin.H{"error": fmt.Sprintf("Doctor with ID: %s not found.", id)})
	} else {
		var data entity.Doctor
		data.Name = c.PostForm("name")
		result := storage.DB.Model(doctor).Updates(data)
		if result.RowsAffected == 0 {
			c.IndentedJSON(http.StatusNotModified, gin.H{"error": result.Error})
		} else {
			storage.DB.Find(&doctor, "id = ?", id)
			c.IndentedJSON(http.StatusOK, doctor)
		}
	}
}

func DeleteDoctor(c *gin.Context) {

	var doctor entity.Doctor

	id := c.Param("id")
	result := storage.DB.Delete(doctor, "id = ?", id)
	if result.RowsAffected == 0 {
		c.IndentedJSON(http.StatusNotFound, gin.H{"error": fmt.Sprintf("Doctor with ID: %s not found.", id)})
	} else {
		c.IndentedJSON(http.StatusResetContent, gin.H{"deleted": result.RowsAffected})
	}
}
