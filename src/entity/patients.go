package entity

import (
	"ExampleCodeMVCApi/src/storage"
)

type Patient struct {
	Id    uint8  `json:"id" gorm:"primary_key"`
	Name  string `json:"name"`
	Email string `json:"email"`
}

func (patient Patient) Create() error {
	result := storage.DB.Create(&patient)
	if result.Error != nil {
		return result.Error
	} else {
		storage.DB.Last(&patient)
		return nil
	}
}

func (patient Patient) Delete() error {
	result := storage.DB.Delete(patient, "id = ?", patient.Id)
	if result.RowsAffected == 0 {
		return result.Error
	} else {
		return nil
	}
}
