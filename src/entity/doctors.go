package entity

type Doctor struct {
	Id    uint8  `json:"id" gorm:"primary_key"`
	Name  string `json:"name"`
	Email string `json:"email"`
}
