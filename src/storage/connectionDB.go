package storage

import (
	"fmt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"log"
	"os"
)

var DB *gorm.DB

//ConnectDB connect to Postgres DB
func ConnectDB() {
	//Connect to DB
	dsn := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", os.Getenv("POSTGRES_HOST"), os.Getenv("POSTGRES_PORT"), os.Getenv("POSTGRES_USER"), os.Getenv("POSTGRES_PASSWORD"), os.Getenv("POSTGRES_DB"))
	database, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})

	//Check for Errors in DB
	if err != nil {
		log.Fatalf("Error in connect the DB %v", err)
	}

	if database.Error != nil {
		log.Fatalln("Any Error in connect the DB " + err.Error())
	}
	log.Println("DB connected")
	DB = database
}
