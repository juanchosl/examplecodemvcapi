package integration

import (
	"ExampleCodeMVCApi/src/entity"
	"ExampleCodeMVCApi/tests"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"testing"
)

var doctor entity.Doctor
var domain = tests.RemoteUrlInit()

func TestPostDoctor(t *testing.T) {
	values := make(url.Values)
	values.Add("name", "Nombre Doctor")
	values.Add("email", "email_Doctor@domminio.com")
	t.Logf("%s", domain)
	req, err := http.PostForm(fmt.Sprintf("%s/doctores", domain), values)
	if err != nil {
		t.Errorf("could not created request: %v", err)
	}
	if req.StatusCode != http.StatusOK {
		t.Errorf("Status code: desired %d, received %d", http.StatusOK, req.StatusCode)
	}
	json.NewDecoder(req.Body).Decode(&doctor)
	t.Logf("Name: %d", doctor.Id)
}
func TestGetDoctor(t *testing.T) {
	t.Logf("doctor id: %d", doctor.Id)
	req, err := http.Get(fmt.Sprintf("%s/doctores/%d", domain, doctor.Id))
	if err != nil {
		t.Errorf("could not created request: %v", err)
	}
	if req.StatusCode != http.StatusOK {
		t.Errorf("Status code: desired %d, received %d", http.StatusOK, req.StatusCode)
	}
	var doctor entity.Doctor
	json.NewDecoder(req.Body).Decode(&doctor)
	t.Logf("Id: %d", doctor.Id)
}

func TestGetDoctors(t *testing.T) {
	req, err := http.Get(fmt.Sprintf("%s/doctores", domain))
	if err != nil {
		t.Fatalf("could not created request: %v", err)
	}

	if req.StatusCode != http.StatusOK {
		t.Fatalf("Status code: desired %d, received %d", http.StatusOK, req.StatusCode)
	}

	var doctors []entity.Doctor
	json.NewDecoder(req.Body).Decode(&doctors)
	len := len(doctors)
	if len == 0 {
		t.Fatalf("No elements %d", len)
	} else {
		t.Logf("Elements: %d", len)
	}

	/*
		fmt.Printf("%v\n", req.Body)
		var doctors []entity.doctor
		json.NewDecoder(req.Body).Decode(&doctors)
		fmt.Printf("%v\n", doctors)
		body, err := ioutil.ReadAll(req.Body)
		fmt.Printf("%v\n", string(body))
		json.Unmarshal([]byte(string(body)), &doctors)
		fmt.Printf("%v\n", doctors)
		/*
			body, err := ioutil.ReadAll(req.Body)
			json.Unmarshal(body, &doctors)
			for _, value := range doctors {
				t.Logf("eleemnt: %v", value)
				if value.Id == 1 {
				}
			}
			return
			t.Logf("created request: %v", string(body))
		return
	*/
}

func TestDeleteDoctor(t *testing.T) {
	t.Logf("doctor id: %d", doctor.Id)
	client := http.DefaultClient
	request, _ := http.NewRequest(http.MethodDelete, fmt.Sprintf("%s/doctores/%d", domain, doctor.Id), nil)
	req, err := client.Do(request)
	if err != nil {
		t.Fatalf("could not created request: %v", err)
	}

	if req.StatusCode != http.StatusResetContent {
		t.Fatalf("Status code: desired %d, received %d", http.StatusResetContent, req.StatusCode)
	}
}
