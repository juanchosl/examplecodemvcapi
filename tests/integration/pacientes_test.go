package integration

import (
	"ExampleCodeMVCApi/src/entity"
	"ExampleCodeMVCApi/tests"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"testing"
)

var patient entity.Patient
var domain_url = tests.RemoteUrlInit()

func TestPostPaciente(t *testing.T) {
	values := make(url.Values)
	values.Add("name", "Nombre paciente")
	values.Add("email", "email_paciente@domminio.com")
	req, err := http.PostForm(fmt.Sprintf("%s/pacientes", domain_url), values)
	if err != nil {
		t.Errorf("could not created request: %v", err)
	}
	if req.StatusCode != http.StatusOK {
		t.Errorf("Status code: desired %d, received %d", http.StatusOK, req.StatusCode)
	}
	json.NewDecoder(req.Body).Decode(&patient)
	t.Logf("Name: %d", patient.Id)
}
func TestGetPaciente(t *testing.T) {
	t.Logf("Patient id: %d", patient.Id)
	req, err := http.Get(fmt.Sprintf("%s/pacientes/%d", domain_url, patient.Id))
	if err != nil {
		t.Errorf("could not created request: %v", err)
	}
	if req.StatusCode != http.StatusOK {
		t.Errorf("Status code: desired %d, received %d", http.StatusOK, req.StatusCode)
	}
	var patient entity.Patient
	json.NewDecoder(req.Body).Decode(&patient)
	t.Logf("Id: %d", patient.Id)
}

func TestGetPacientes(t *testing.T) {
	req, err := http.Get(fmt.Sprintf("%s/pacientes", domain_url))
	if err != nil {
		t.Fatalf("could not created request: %v", err)
	}

	if req.StatusCode != http.StatusOK {
		t.Fatalf("Status code: desired %d, received %d", http.StatusOK, req.StatusCode)
	}

	var patients []entity.Patient
	json.NewDecoder(req.Body).Decode(&patients)
	len := len(patients)
	if len == 0 {
		t.Fatalf("No elements %d", len)
	} else {
		t.Logf("Elements: %d", len)
	}

	/*
		fmt.Printf("%v\n", req.Body)
		var patients []entity.Patient
		json.NewDecoder(req.Body).Decode(&patients)
		fmt.Printf("%v\n", patients)
		body, err := ioutil.ReadAll(req.Body)
		fmt.Printf("%v\n", string(body))
		json.Unmarshal([]byte(string(body)), &patients)
		fmt.Printf("%v\n", patients)
		/*
			body, err := ioutil.ReadAll(req.Body)
			json.Unmarshal(body, &patients)
			for _, value := range patients {
				t.Logf("eleemnt: %v", value)
				if value.Id == 1 {
				}
			}
			return
			t.Logf("created request: %v", string(body))
		return
	*/
}

func TestDeletePaciente(t *testing.T) {
	client := http.DefaultClient
	request, _ := http.NewRequest(http.MethodDelete, fmt.Sprintf("%s/pacientes/%d", domain_url, patient.Id), nil)
	req, err := client.Do(request)
	if err != nil {
		t.Fatalf("could not created request: %v", err)
	}

	if req.StatusCode != http.StatusResetContent {
		t.Fatalf("Status code: desired %d, received %d", http.StatusResetContent, req.StatusCode)
	}
}
