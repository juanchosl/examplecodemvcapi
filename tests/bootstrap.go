package tests

import (
	"ExampleCodeMVCApi/src/storage"
	"fmt"
	"github.com/joho/godotenv"
	"log"
	"os"
)

func EnvInit() {
	err := godotenv.Load("../../.env")
	if err != nil {
		log.Fatalf("Some error occured. Err: %s", err)
	}
}

func RemoteUrlInit() string {
	EnvInit()
	return fmt.Sprintf("http://%s:%s", os.Getenv("GOAPP_DOMAIN"), os.Getenv("GOAPP_PORT"))
}

func DBInit() {
	EnvInit()
	storage.ConnectDB()
}
