package unitary

import (
	"ExampleCodeMVCApi/src/entity"
	"ExampleCodeMVCApi/src/storage"
	"ExampleCodeMVCApi/tests"
	"testing"
)

var patient entity.Patient

func setup() {
	tests.DBInit()
}

func TestCreatePaciente(t *testing.T) {
	setup()
	patient := entity.Patient{
		Name:  "Paciente de prueba",
		Email: "Email del paciente de prueba",
	}
	result := storage.DB.Create(&patient)
	if result.RowsAffected == 0 {
		t.Errorf("Row not created")
	} else if result.Error != nil {
		t.Errorf("Error %s", result.Error)
	} else {
		t.Logf("Created Id: %d", patient.Id)
	}
}

func TestReadPacientes(t *testing.T) {
	setup()
	var patients []entity.Patient
	result := storage.DB.Find(&patients)
	if result.RowsAffected == 0 {
		t.Errorf("Row not founded")
	} else if result.Error != nil {
		t.Error(result.Error)
	} else {
		t.Log(result.RowsAffected)
	}
}
func TestReadPaciente(t *testing.T) {
	setup()
	storage.DB.Last(&patient)
	result := storage.DB.Find(&patient, "id = ?", patient.Id)
	if result.RowsAffected == 0 {
		t.Errorf("Row not founded")
	} else if result.Error != nil {
		t.Error(result.Error)
	}
}

func TestPatchPaciente(t *testing.T) {
	setup()
	storage.DB.Last(&patient)
	t.Logf("Last: %d", patient.Id)
	patient.Name = "Paciente de prueba cambiado"
	result := storage.DB.Updates(patient)
	if result.RowsAffected == 0 {
		t.Errorf("Row not updated")
	} else if result.Error != nil {
		t.Errorf("Error %s", result.Error)
	} else {
		t.Logf("Name: %s", patient.Name)
		//storage.DB.Last(&patient)
	}
}

func TestDeletePaciente(t *testing.T) {
	setup()
	storage.DB.Last(&patient)
	result := storage.DB.Delete(patient)
	if result.RowsAffected == 0 {
		t.Errorf("Row not deleted")
	} else if result.Error != nil {
		t.Errorf("Error %s", result.Error)
	} else {
		t.Logf("Deleted %d", patient.Id)
		//storage.DB.Last(&patient)
	}
}
