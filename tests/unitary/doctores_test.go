package unitary

import (
	"ExampleCodeMVCApi/src/entity"
	"ExampleCodeMVCApi/src/storage"
	"ExampleCodeMVCApi/tests"
	"testing"
)

var doctor entity.Doctor

func setupDb() {
	tests.DBInit()
}

func TestCreateDoctor(t *testing.T) {
	setupDb()
	doctor := entity.Doctor{
		Name:  "Doctor de prueba",
		Email: "Email del Doctor de prueba",
	}
	result := storage.DB.Create(&doctor)
	if result.RowsAffected == 0 {
		t.Errorf("Row not created")
	} else if result.Error != nil {
		t.Errorf("Error %s", result.Error)
	} else {
		t.Logf("Created Id: %d", doctor.Id)
	}
}

func TestReadDoctores(t *testing.T) {
	setupDb()
	var doctors []entity.Doctor
	result := storage.DB.Find(&doctors)
	if result.RowsAffected == 0 {
		t.Errorf("Row not founded")
	} else if result.Error != nil {
		t.Error(result.Error)
	} else {
		t.Log(result.RowsAffected)
	}
}
func TestReadDoctor(t *testing.T) {
	setupDb()
	storage.DB.Last(&doctor)
	result := storage.DB.Find(&doctor, "id = ?", doctor.Id)
	if result.RowsAffected == 0 {
		t.Errorf("Row not founded")
	} else if result.Error != nil {
		t.Error(result.Error)
	}
}

func TestPatchDoctor(t *testing.T) {
	setupDb()
	storage.DB.Last(&doctor)
	t.Logf("Last: %d", doctor.Id)
	doctor.Name = "Doctor de prueba cambiado"
	result := storage.DB.Updates(doctor)
	if result.RowsAffected == 0 {
		t.Errorf("Row not updated")
	} else if result.Error != nil {
		t.Errorf("Error %s", result.Error)
	} else {
		t.Logf("Name: %s", doctor.Name)
		//storage.DB.Last(&doctor)
	}
}

func TestDeleteDoctor(t *testing.T) {
	setupDb()
	storage.DB.Last(&doctor)
	result := storage.DB.Delete(doctor)
	if result.RowsAffected == 0 {
		t.Errorf("Row not deleted")
	} else if result.Error != nil {
		t.Errorf("Error %s", result.Error)
	} else {
		t.Logf("Deleted %d", doctor.Id)
		//storage.DB.Last(&doctor)
	}
}
