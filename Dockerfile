FROM golang:1.16.2-alpine
ENV GO111MODULE=on
WORKDIR /tmp/app
COPY . .
RUN go mod download
EXPOSE 8080
ENTRYPOINT go run ./cmd/main.go