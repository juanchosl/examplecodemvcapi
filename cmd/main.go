package main

import (
	"ExampleCodeMVCApi/src/actions"
	"ExampleCodeMVCApi/src/storage"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"log"
	"os"
	"path"
	"runtime"
)

func Init() {
	_, filename, _, _ := runtime.Caller(1)
	pwd := path.Dir(filename)
	//log.Fatal(pwd)
	err := godotenv.Load(pwd + "/../.env")
	if err != nil {
		log.Fatalf("Some error occured. Err: %s", err)
	}
	storage.ConnectDB()
}

func main() {
	Init()
	router := gin.Default()
	router.GET("/doctores", actions.GetDoctors)
	router.GET("/doctores/:id", actions.GetDoctor)
	router.POST("/doctores", actions.SetDoctor)
	router.PATCH("/doctores/:id", actions.UpdateDoctor)
	router.DELETE("/doctores/:id", actions.DeleteDoctor)

	router.GET("/pacientes", actions.GetPersons)
	router.GET("/pacientes/:id", actions.GetPerson)
	router.POST("/pacientes", actions.SetPerson)
	router.PATCH("/pacientes/:id", actions.UpdatePerson)
	router.DELETE("/pacientes/:id", actions.DeletePerson)

	//router.Run("0.0.0.0:8080")
	router.Run(fmt.Sprintf(":%s", os.Getenv("GOAPP_PORT")))
}
