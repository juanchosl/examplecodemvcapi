## Developer
Juan Sánchez Lecegui <JuanchoSL@hotmail.com>

## Descripción
El presente proyecto crea y habilita una RestFull API para poder gestionar los médicos y pacientes de una base de datos.

Tenemos los siguientes endpoints disponibles

* GET {host}/doctores -> para recuperar los doctores
* GET {host}/doctores/{id} -> para recuperar 1 sólo doctor
* POST {host}/doctores -> para crear 1 doctor pasando los valores **name** y **email**
* PATCH {host}/doctores/{id} -> para editar 1 doctor pasando el nuevo valor de **name**
* DELETE {host}/doctores/{id} -> para eliminar 1 doctor

* GET {host}/pacientes -> para recuperar los pacientes
* GET {host}/pacientes/{id} -> para recuperar 1 sólo paciente
* POST {host}/pacientes -> para crear 1 paciente pasando los valores **name** y **email**
* PATCH {host}/pacientes/{id} -> para editar 1 paciente pasando el nuevo valor de **name**
* DELETE {host}/pacientes/{id} -> para eliminar 1 paciente

## Sistema
El proceso está creado y probado usando GoLang 1.16 y Postgres 9

## Dependencias
Este proyecto usa gin-gonic para la gestión de las Request y GORM para trabajar con la base de datos.

## Ejecución local
```
docker-compose -f docker-compose.yaml.local up -d
```
```
go run ./cmd/main.go
```


## Ejecución completa docker (database + Go server)
```
docker-compose -f docker-compose.yaml up -d 
```

## Crear tablas (primer vez sólo en docker deploy)
```
migrate -database "postgres://{user}:{pass}@{host}:{port}}/{database}?sslmode=disable" -path db/migrations up
```
